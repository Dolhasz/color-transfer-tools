import argparse
import os
import cv2
import numpy as np
import matplotlib.pyplot as plt 


def parse_args():
	parser = argparse.ArgumentParser()
	parser.add_argument('--source', help='Image whose color we want to change')
	parser.add_argument('--target', help='Image whose color we want to use as example')
	return parser.parse_args()


def load_images(source_path, target_path):
	s = cv2.imread(source_path)
	s = cv2.cvtColor(s,cv2.COLOR_BGR2LAB)
	t = cv2.imread(target_path)
	t = cv2.cvtColor(t,cv2.COLOR_BGR2LAB)
	return s, t


def to_RGB(img):
	return cv2.cvtColor(img, cv2.COLOR_LAB2RGB)


def get_mean_and_std(x, mask=None):
	x_mean, x_std = cv2.meanStdDev(x, mask=mask)
	x_mean = np.hstack(np.around(x_mean,2))
	x_std = np.hstack(np.around(x_std,2))
	return x_mean, x_std


def color_transfer(source, target, source_mask=None, target_mask=None):
	source = (source*255).astype(np.uint8)
	source_mask = (source_mask*255).astype(np.uint8)
	target = (target*255).astype(np.uint8)
	target_mask = (target_mask*255).astype(np.uint8)
	original = source.copy()

	source = cv2.cvtColor(source, cv2.COLOR_RGB2LAB)
	target = cv2.cvtColor(target, cv2.COLOR_RGB2LAB)


	s_mean, s_std = get_mean_and_std(source, source_mask)
	t_mean, t_std = get_mean_and_std(target, target_mask)

	height, width, channel = source.shape
	for i in range(0,height):
		for j in range(0,width):
			for k in range(0,channel-2):
				if source_mask is not None and target_mask is not None:
					if source_mask[i,j] == 0:
						continue
					else:
						x = source[i,j,k]
						x = ((x-s_mean[k])*(t_std[k]/s_std[k]))+t_mean[k]
						x = round(x)
						x = 0 if x<0 else x
						x = 255 if x>255 else x
						source[i,j,k] = x

	result = to_RGB(source)
	target = to_RGB(target)
	return result, original, target


def transfer_pair(source, target):
	source, target = load_images(source, target)
	result = color_transfer(source, target)
	return result


if __name__ == "__main__":
	args = parse_args()
	result, original, target = transfer_pair(args.source, args.target)

	f, ax = plt.subplots(1,3)
	ax[0].imshow(result)
	ax[1].imshow(to_RGB(original))
	ax[2].imshow(to_RGB(target))
	plt.show()

	
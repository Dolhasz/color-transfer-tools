'''Sample and apply brightness transformations to images from COCO'''

import datetime
import cv2
import os
import numpy as np 
import skimage.io as io
from random import shuffle
from pycocotools.coco import COCO
from keras.models import load_model
import matplotlib
import matplotlib.pyplot as plt

from color_transfer import color_transfer

def imread(fname, shape):
    return cv2.resize(io.imread(fname) / 255.0, shape[:2])

def preprocess_input(x):
    if isinstance(x, list):
        return [img * 2 - 1.0 for img in x]
    else:
        return x * 2.0 - 1.0


class CocoSequence:
    def __init__(self, batch_size=32, image_shape=(224, 224, 3), data_dir='E:/_________COCO_________',
            data_type='train2017', n_samples=None):
        annotation_file = f'{data_dir}/annotations/instances_{data_type}.json'
        self.data_dir = data_dir
        self.data_type = data_type
        self.batch_size = batch_size
        self.image_shape = image_shape
        
        # Initialize COCO
        self.coco = COCO(annotation_file)
        self.categories = [c['name'] for c in self.coco.loadCats(self.coco.getCatIds())]

        if n_samples is not None:
            self.x = self.coco.getImgIds()[:n_samples]
        else:
            self.x = self.coco.getImgIds()
        shuffle(self.x)
        self.n_images = len(self.x)


    def sample(self):
        category = np.random.choice(self.categories)
        cat_ids = self.coco.getCatIds(catNms=category)
        cat_imgs = self.coco.getImgIds(catIds=cat_ids)
        img_ids = self.coco.getImgIds(imgIds=cat_imgs)
        img1 = self.coco.loadImgs([np.random.choice(img_ids)])[0]
        img2 = self.coco.loadImgs([np.random.choice(img_ids)])[0]
        im1 = self.load_img(img1) 
        im2 = self.load_img(img2) 
        m1 = self.load_mask(img1, cat_ids)
        m2 = self.load_mask(img2, cat_ids)

        return im1, im2, m1, m2


    def sample_image(self):
        im1, im2, m1, m2 = self.sample()
        while (np.count_nonzero(m1) < 22 or np.count_nonzero(m2) < 22) or ((im1.ndim < 3) or (im2.ndim < 3)):
            im1, im2, m1, m2 = self.sample()
        return im1, im2, m1, m2


    def load_img(self, coco_img):
        fname = f'{self.data_dir}/images/{self.data_type}/{coco_img["file_name"]}'
        np_img = imread(fname, self.image_shape)
        return np_img


    def load_mask(self, coco_img, cat_id):
        ann_ids = self.coco.getAnnIds(imgIds=coco_img['id'], catIds=cat_id, iscrowd=None)
        anns = self.coco.loadAnns(ann_ids)
        if len(anns) < 1:
            return None
        mask = cv2.resize(
            self.coco.annToMask(np.random.choice(anns)),
            self.image_shape[:2]
            )
        return mask.astype(np.float32)


def predict(model, image):
    image = image / np.max(image)
    preproc = preprocess_input(image)
    pred = model.predict(preproc.reshape(-1, 224, 224, 3))[0]
    return pred.reshape(224,224,3)


def save_img(fname, img):
    img = cv2.cvtColor(img, cv2.COLOR_RGB2BGR)
    cv2.imwrite(fname, img)



if __name__ == "__main__":
    start = datetime.datetime.now()
    gen = CocoSequence(1, image_shape=(224,224,3), data_dir='E:/_________COCO_________')
    dest_dir = 'F:/visapp_data/'
    for idx in range(gen.n_images):
        im1, im2, m1, m2 = gen.sample_image()
        result, source, target = color_transfer(im1, im2, m1, m2)
        
        fname = f'image_{idx}.png'
        mfname = f'mask_{idx}.png'
        cfname = f'comp_{idx}.png'

        save_img(os.path.join(dest_dir, fname), source)
        save_img(os.path.join(dest_dir, cfname), result)
        save_img(os.path.join(dest_dir, mfname), (m1*255).astype(np.uint8))
    print(datetime.datetime.now()-start)
    
    
    # matplotlib.use('TkAgg')
    # for _ in range(100):
    #     im1, im2, m1, m2 = gen.sample_image()

    #     result, source, target = color_transfer(im1, im2, m1, m2)
    #     model = load_model('C:/Users/Alan/Documents/threshold-learning/training_results/downloaded/weights.885-0.05.hdf5', compile=False)

    #     f, ax = plt.subplots(1,7)
    #     ax[0].imshow(result)
    #     ax[0].set(xticks=[], yticks=[], xlabel='Transferred')
    #     ax[1].imshow(im1)
    #     ax[1].set(xticks=[], yticks=[], xlabel='Source')
    #     ax[2].imshow(im2)
    #     ax[2].set(xticks=[], yticks=[], xlabel='Target')
    #     ax[3].imshow(m1)
    #     ax[3].set(xticks=[], yticks=[], xlabel='Source Mask')
    #     ax[4].imshow(m2)
    #     ax[4].set(xticks=[], yticks=[], xlabel='Target Mask')
    #     ax[5].imshow(predict(model, im1), vmin=0.0, vmax=1.0)
    #     ax[5].set(xticks=[], yticks=[], xlabel='MY NETWORK (unproc)')
    #     ax[6].imshow(predict(model, result), vmin=0.0, vmax=1.0)
    #     ax[6].set(xticks=[], yticks=[], xlabel='MY NETWORK (proc)')

    #     plt.show()

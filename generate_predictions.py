import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from keras.models import load_model
from coco_sampler import imread, preprocess_input

def generate_dh_preds(model_path, input_dir, output_dir, result_dir):
    '''
    Generate predictions for COMPOSITES which have also been fixed by DH.
    These prediction masks should then be fed to the fixer again, along with composites
    '''
    model = load_model(model_path, compile=False)
    image_list = [path for path in os.listdir(result_dir) if path.endswith('.png')]
    n_images = len(image_list)
    for idx, image_file in enumerate(image_list):
        print(f'Processing image {idx}/{n_images}', end='\r')
        image_path = os.path.join(input_dir, image_file)
        image = imread(image_path)
        image = preprocess_input(image)
        prediction = np.squeeze(model.predict(np.expand_dims(image, 0))[0])
        dest_path = os.path.join(output_dir, image_file)
        plt.imsave(dest_path, prediction)



if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--composites')
    parser.add_argument('--output')
    parser.add_argument('--filter')
    parser.add_argument('--model')
    args = parser.parse_args()

    generate_dh_preds(args.model, args.composites, args.output, args.filter)